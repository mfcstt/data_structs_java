package encadeado;

public class Frete {
    private String placa;
    private String motorista;
    private String carga;

    public String getPlaca() {
        return placa;
    }
    public String getMotorista() {
        return motorista;
    }
    public String getCarga() {
        return carga;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    public void setMotorista(String motorista) {
        this.motorista = motorista;
    }
    public void setCarga(String carga) {
        this.carga = carga;
    }
    public Frete(String placa, String motorista, String carga) {
        this.placa = placa;
        this.motorista = motorista;
        this.carga = carga;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((placa == null) ? 0 : placa.hashCode());
        result = prime * result + ((motorista == null) ? 0 : motorista.hashCode());
        result = prime * result + ((carga == null) ? 0 : carga.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Frete other = (Frete) obj;
        if (placa == null) {
            if (other.placa != null)
                return false;
        } else if (!placa.equals(other.placa))
            return false;
        if (motorista == null) {
            if (other.motorista != null)
                return false;
        } else if (!motorista.equals(other.motorista))
            return false;
        if (carga == null) {
            if (other.carga != null)
                return false;
        } else if (!carga.equals(other.carga))
            return false;
        return true;
    }

    

    

    

}
