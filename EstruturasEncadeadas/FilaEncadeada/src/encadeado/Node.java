package encadeado;

public class Node<N> {
    private   N info;
    private Node< N> next;
    
    public Node(  N info) {
        this.info = info;
    }

    public N getInfo() {
        return info;
    }
    public void setInfo(  N info) {
        this.info = info;
    }
    public Node<  N> getNext() {
        return next;
    }
    public void setNext(Node< N> next) {
        this.next = next;
    }
    
}