package encadeado;

public class Main {
    private static FilaEncadeada<Frete> minhaFila;
    
    public static void main(String[] args) throws Exception {
        minhaFila = new FilaEncadeada<Frete>(5);

        minhaFila.insere(new Frete("ABC-1234", "Luciano", "Carga 1"));
        minhaFila.insere(new Frete("DEF-5678", "Maria", "Carga 2"));
        minhaFila.insere(new Frete("GHI-9101", "João", "Carga 3"));

        System.out.println("Tamanho da fila: " + minhaFila.tamanho());
    }
}
