package encadeado;

public class FilaEncadeada<N>{
        private Node <N> inicio;
        private Node <N> fim;

        private int elementos;
        private int max;
    
        public FilaEncadeada() {
            inicio = null;
            fim = null;
            elementos = 0;
        }
    
        
        public int tamanho() {
            return elementos;
        }

        public FilaEncadeada(int max) {
            this();
            this.max = max;
        }

        public boolean estaVazia(){
            return elementos == 0;
        }

        public boolean estaCheia(){
            return elementos == max;
        }
    
        public void insere(N info) throws Exception{
            Node<N> novo = new Node<N>(info);
            if(estaVazia()){
                inicio = novo;
                fim = novo;
                elementos ++;
            }
            else{
               if(!estaCheia()){
                    fim.setNext(novo);
                    fim = novo;
                    elementos ++;
               }
                else{
                     throw new Exception("Fila cheia");
                }
            } 
        }
    
        public void insereFim(N info) throws Exception{
            Node<N> novo = new Node<N>(info);
            if(estaVazia()){
                insere(info);
            }
            else{
                fim.setNext(novo);
                fim = novo;
                elementos ++;
            }
            throw new Exception("Fila cheia");
        }

        public N remove() throws Exception{
            if(estaVazia()){
                throw new Exception("Fila vazia");
            }
            N info = inicio.getInfo();
            elementos--;
            inicio = inicio.getNext();
            
            if(estaVazia()){
                fim = inicio;
            }
            return info;
        }
        
    

}