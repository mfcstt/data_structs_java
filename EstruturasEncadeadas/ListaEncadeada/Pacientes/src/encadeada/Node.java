package encadeada;
public class Node<P> {
    private P info;
    private Node<P> next;

    
    public Node(P info) {
        this.info = info;
    }

    public P getInfo() {
        return info;
    }
    public void setInfo(P info) {
        this.info = info;
    }
    public Node<P> getNext() {
        return next;
    }
    public void setNext(Node<P> next) {
        this.next = next;
    }
    


    
}
