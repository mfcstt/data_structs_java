package encadeada;

public class Main {

    private static Functions<Paciente> normal;
    private static Functions<Paciente> prioritario;

    public static void main(String[] args) throws Exception {
        
        normal = new Functions<Paciente>();
        prioritario = new Functions<>();
        
        normal.add(new Paciente("Eduardo", 20));
        normal.add(new Paciente("Gui", 29));
        prioritario.add(new Paciente("João", 30));

        System.out.println("Normal: " + normal.tamanho());
        System.out.println("Prioritário: " + prioritario.tamanho());

        System.exit(0);
    }
}