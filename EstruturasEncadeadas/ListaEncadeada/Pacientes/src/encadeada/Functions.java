package encadeada;
public class Functions<L> {
    private Node <L> first;
    private int elements = 0;

    public Functions() {
        first = null;
    }

    
    public int tamanho() {
        return elements;
    }

    public void add(L info){
        if(first == null){
            first = new Node<L>(info);
        }
        else{
            Node<L> aux = first;
            while (aux.getNext() != null) {
            aux = aux.getNext();
                
            }
            aux = new Node<L>(info);
        }
        elements++;

    }

    public void addBegin(L info){
        Node <L> newNode = new Node<L>(info);
        elements++;

        if(first != null){ 
        newNode.setNext(first);
        }
        first = newNode;
    }

    
}
