package circular;


public class Atendimento {
    public static void main(String[] args) throws Exception {
        
        System.out.println("Bem vindo ao sistema de atendimento!");

        boolean sair = false;
        while (!sair) {

             ImprimirMenu.ImprimirMenu();
            
            switch (ImprimirMenu.opcaoEscolhida){
                case 1:
                    FunçõesAtendimento.inserirPaciente();
                    break;
                case 2:
                    FunçõesAtendimento.chamarPaciente();
                    break;
                case 3:
                    FunçõesAtendimento.imprimirFilaAtendimento();
                    break;
                case 4:
                    System.out.println("Saindo do sistema");
                    sair = true;
                    break;
                default:
                    System.out.println("Opção inválida");
                    break;
            }
        }
    }
}
    