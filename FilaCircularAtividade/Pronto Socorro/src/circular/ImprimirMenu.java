package circular;

import validacao.Validar;

public class ImprimirMenu {
    public static int opcaoEscolhida = 0;

    public static void ImprimirMenu(){

        System.out.println("Escolha uma opção:");
    
        opcaoEscolhida = Validar.validaOpcaoMenu(
        "  1 - Inserir paciente na fila de atendimento" + "\n" + 
        "  2 - Chamar paciente" + "\n" +
        "  3 - Imprimir fila de atendimento" + "\n" + 
        "  4 - Sair do sistema",
         "Digite um valor inteiro!", 
         true);
    }
}
