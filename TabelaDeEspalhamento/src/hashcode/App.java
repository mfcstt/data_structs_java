package hashcode;

import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        int quantidadeValores = 10;
        FunçãoEspalhamento.criarTabela(quantidadeValores);

        int elementos = 0;

        while(elementos <= quantidadeValores) {
           int dado = (int) (Math.random() * 100);
           if(FunçãoEspalhamento.buscarDado(dado) == 0) {
               FunçãoEspalhamento.inserir(dado);
               elementos++;
           }
        }
        int[] tabela = FunçãoEspalhamento.exibirTabela();
        int i = 0;
        for (int dado : tabela) {
            System.out.print("indice-> " + (i++) + " valor-> ");
            System.out.println(dado + " key-> " + (dado % tabela.length));
        }
        
        Scanner teclado = new Scanner(System.in);
        System.out.println("Digite um valor para buscar na tabela: ");
        int valor = teclado.nextInt();
        System.out.println("Busca por espalhamento");
        if(FunçãoEspalhamento.buscarDado(valor) == 0) {
                System.out.println("Valor não encontrado");
            } else {
                System.out.println("Valor encontrado");
                
            }
            
            
        }
    }
    
