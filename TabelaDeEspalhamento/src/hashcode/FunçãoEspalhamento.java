package hashcode;

public class FunçãoEspalhamento {

  private static int tabela[];
  private static int tamanhoTabela;

  public static void criarTabela(int quantidadeValores) {
    int totalTabela = quantidadeValores * 2;
    while (!verificarPrimo(totalTabela)) {
      totalTabela++;
    }
    tamanhoTabela = totalTabela;
    tabela = new int[tamanhoTabela];
  }

  private static boolean verificarPrimo(int valor) {
    for (int i = 2; i < valor; i++) {
      if (valor % i == 0) {
        return false;
      }
    }
    return true;
  }

  private static int fatorHash(int chave) {
    return (chave % tamanhoTabela);
  }

  public static void inserir(int dado) {
    int id = fatorHash(dado);
    while (true) {
      if (tabela[id] == 0) {
        tabela[id] = dado;
        break;
      }
      id = fatorHash(id + 1);
    }
  }

  public static int buscarDado(int dado) {
    int id = fatorHash(dado);
    int leitura = 0;
    while (true) {
      leitura++;
      if (tabela[id] == dado) {
        System.out.println("Leituras: " + leitura);
        return tabela[id];
      } else 
          if (tabela[id] == 0) {
        System.out.println("Não Localizado em Leituras: " + leitura);
        break;
      } else {
        id = fatorHash(id + 1);
      }
    }
    return 0;
  }

  public static int[] exibirTabela() {
    return tabela;
  }

}